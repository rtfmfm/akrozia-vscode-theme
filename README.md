# Akrozia Theme for Visual Studio Code

Color theme based on Setu_UX theme for Sublime text

Homepage: https://bitbucket.org/rtfmfm/akrozia-vscode-theme<br>
Original Sublime Text theme: https://github.com/ctf0/Seti_UX

## Screenshot
![](https://bitbucket.org/rtfmfm/akrozia-vscode-theme/raw/4c3ef27c0686b9fc63af7c54b0cecd1a026e89f7/images/screenshot.png)

